import express from "express";
import OpenAI from "openai";
import * as fs from "fs";
import cors from "cors";
import 'dotenv/config'
import bp from "body-parser";


const openai = new OpenAI();
const app = express();

app.use(cors());


app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))
app.listen(process.env.PORT,()=>{
    console.log('server listening on port '+process.env.PORT);
    try {
        console.log('connection succeded');
    } catch (error) {
        console.log('connection error');
        console.log(error);
    }
})

app.post("/generate-story", (req, res) => {
    const personnage = req.body.personnage;
    openai.chat.completions.create({
        model: "gpt-3.5-turbo",
        messages: [{
            "role": "system",
            "content": "generate a story for a fictional fantasy creature. The story must be less than 3000 characters long."
        }],
    }).then((data) => {
        res.json({ description: data.choices[0].message.content });
    }).catch((error) => {
        console.error("Erreur lors de la génération de l'histoire :", error);
        res.status(500).send("Erreur interne du serveur");
    });
});

