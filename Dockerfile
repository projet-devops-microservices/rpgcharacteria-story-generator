FROM node
WORKDIR /usr/src/story-generator
COPY . /usr/src/story-generator
RUN npm i
EXPOSE 3003
CMD npm run start --host 0.0.0.0

